<?php

/**
 * @copyright 2024 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/mock-credential-store/-/blob/main/LICENSE MIT
 */

declare(strict_types=1);

namespace Xint0\MockCredentialStore;

use Xint0\CredentialStorage\Contracts\CredentialInterface;

/**
 * An implementation of the Credential interface that is useful for automated tests.
 *
 * This mock does not encrypt secrets, it simply wraps the value with `encrypted()`.
 *
 * @author Rogelio Jacinto <rogelio.jacinto@gmail.com>
 */
class Credential implements CredentialInterface
{
    public function __construct(public string $identity, public string $secret)
    {
    }

    /**
     * @inheritDoc
     */
    public function decryptSecret(): string
    {
        return $this->secret;
    }

    /**
     * @inheritDoc
     */
    public function getIdentity(): string
    {
        return $this->identity;
    }

    /**
     * @inheritDoc
     */
    public function getSecret(): string
    {
        return sprintf('encrypted(%s)', $this->secret);
    }
}
