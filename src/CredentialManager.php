<?php

/**
 * @copyright 2024 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/mock-credential-store/-/blob/main/LICENSE MIT
 */

declare(strict_types=1);

namespace Xint0\MockCredentialStore;

use Xint0\CredentialStorage\Contracts\CredentialFactoryExceptionInterface;
use Xint0\CredentialStorage\Contracts\CredentialFactoryInterface;
use Xint0\CredentialStorage\Contracts\CredentialInterface;
use Xint0\CredentialStorage\Contracts\CredentialStoreExceptionInterface;
use Xint0\CredentialStorage\Contracts\CredentialStoreInterface;
use Xint0\CredentialStorage\Exceptions\CredentialNotFoundException;

class CredentialManager implements CredentialStoreInterface, CredentialFactoryInterface
{
    /** @var CredentialInterface[] */
    public array $store = [];

    public ?CredentialFactoryExceptionInterface $createCredentialException = null;
    public ?CredentialStoreExceptionInterface $getCredentialException = null;
    public ?CredentialStoreExceptionInterface $putCredentialException = null;

    /**
     * @param  array<string,CredentialInterface|array{identity?:string,secret?:string}|array<int,string>>  $credentials
     */
    public function __construct(array $credentials = [])
    {
        $this->initializeStore($credentials);
    }

    /**
     * @inheritDoc
     */
    public function getCredential(string $name): CredentialInterface
    {
        if (null !== $this->getCredentialException) {
            throw $this->getCredentialException;
        }

        if (array_key_exists($name, $this->store)) {
            return $this->store[$name];
        }

        throw new CredentialNotFoundException($name);
    }

    /**
     * @inheritDoc
     */
    public function putCredential(string $name, CredentialInterface $credential): void
    {
        if (null !== $this->putCredentialException) {
            throw $this->putCredentialException;
        }

        $this->store[$name] = $credential;
    }

    /**
     * @inheritDoc
     */
    public function createCredential(string $identity, string $secret): CredentialInterface
    {
        if (null !== $this->createCredentialException) {
            throw $this->createCredentialException;
        }
        return new Credential($identity, $secret);
    }

    /**
     * @param  array<string,CredentialInterface|array{identity?:string,secret?:string}|array<int,string>>  $credentials
     *
     * @return void
     */
    private function initializeStore(array $credentials): void
    {
        foreach ($credentials as $name => $credential) {
            if ($credential instanceof CredentialInterface) {
                $this->store[$name] = $credential;
            }
            if (is_array($credential)) {
                $identity = null;
                $secret = null;
                if (array_key_exists('identity', $credential)) {
                    $identity = $credential['identity'];
                }
                if (array_key_exists('secret', $credential)) {
                    $secret = $credential['secret'];
                }
                $values = array_values($credential);
                $value_count = count($values);
                if (null === $identity && $value_count >= 1) {
                    $identity = $values[0];
                }
                if (null === $secret && $value_count >= 2) {
                    $secret = $values[1];
                }
                $this->store[$name] = new Credential($identity ?? '', $secret ?? '');
            }
        }
    }
}
