<?php

declare(strict_types=1);

namespace Tests\Unit;

use PHPUnit\Framework\Attributes\CoversClass;
use Xint0\CredentialStorage\Exceptions\CredentialNotFoundException;
use Xint0\MockCredentialStore\CredentialManager;
use PHPUnit\Framework\TestCase;

#[CoversClass(CredentialManager::class)]
class CredentialManagerTest extends TestCase
{
    public function test_throws_credential_not_found_exception_when_credential_is_not_found(): void
    {
        $sut = new CredentialManager();

        $this->expectException(CredentialNotFoundException::class);
        $this->expectExceptionMessage('Credential "foo" not found.');

        /** @noinspection PhpUnhandledExceptionInspection */
        $sut->getCredential('foo');
    }
}
