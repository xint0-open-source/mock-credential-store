# Mock Credential Store

## Description
A mock credential manager that implements `xint0/credential-storage-contract` interfaces for testing.

## Badges

## Installation
Install using Composer as a development dependency:

```bash
composer require --dev xint0/mock-credential-store
```

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License
The Xint0 Mock Credential Store is open source software licensed under the [MIT License](https://gitlab.com/xint0-open-source/mock-credential-store/-/blob/main/LICENSE).

## Project status
