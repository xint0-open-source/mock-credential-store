# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/xint0-open-source/mock-credential-store/-/compare/1.0.0...main)

### Changed

- Add phpstan docker compose service.
- Add build:phpstan-image ci job.
- Add phpcs and phpcbf to project.
- Add php 8.4 to ci jobs execution matrix.

## [1.0.0](https://gitlab.com/xint0-open-source/mock-credential-store/-/compare/0.1.0...1.0.0) - 2024-09-02

### Changed

- `Xint0\MockCredentialStore\CredentialManager` throws `Xint0\CredentialStorage\Exceptions\CredentialNotFoundException`
when test credential store does not contain the specified credential.

## [0.1.0](https://gitlab.com/xint0-open-source/mock-credential-store/-/tags/0.1.0) - 2024-06-05

### Added

- `Xint0\MockCredentialStore\CredentialManager` implements `Xint0\CredentialStorage\Contracts\CredentialStoreInterface`
and `Xint0\CredentialStorage\Contracts\CredentialFactoryInterface` interfaces.
- `Xint0\MockCredentialStore\Credential` implements `Xint0\CredentialStorage\Contracts\CredentialInterface` interface.